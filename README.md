# Sezzle k8s assignment

# Gitlab
- Create a namespace called gitlab
    ```
    kubectl create ns gitlab
    ```
- Apply redis yaml given in repo
    ```
    kubectl -n gitlab apply -f redis.yaml
    ```
- Apply postgres yaml in given repo
    ```
    kubectl -n gitlab apply -f postgres.yaml
    ```
- Apply gitlab yaml in given repo
    ```
    kubectl -n gitlab apply -f gitlab.yaml
    ```
- To access gitlab use masterip:nodeport
    ```
    kubectl -n gitlab get svc 
    ```

# Jenkins
- Create a namespace called jenkins
    ```
    kubectl create ns jenkins
    ```
- Apply jenkins yaml given in repo. I am using minikube so I have "standard" storageclass. Make sure to chage it accordingly 
    ```
    kubectl -n jenkins apply -f jenkins.yaml
    ```
- Apply jenkins svc yaml given in repo
    ```
    kubectl -n jenkins apply -f jenkins-svc.yaml
    ```
- To access jenkins use masterip:nodeport
    ```
    kubectl -n jenkins get svc
    ```

# Python app
- Build the dockerimage from dockerfile given in repo. Make sure to chage the image tag accordingly
    ```
    docker build -t shubhidnia/sezzle_python .
    ```
- Push the image to contiane registry. I am using dockerhub here
    ```
    docker push shubhidnia/sezzle_python
    ```
- Change the imagename in sezzle-python-app.yaml and apply it. Make sure you have sezzle namesopace created in your cluster
    ``` 
    kubectl -n sezzle apply -f sezzle-python-app.yaml
    ```
    
## Configuring gitlab and jenkins
- Open gitlab from nodeport (masterip:nodeport) and click on register.
- Enter your details and click register.
- User will be created and you can start using gitlab.
- To configure jenkins, we need initial password first. Use kubectl to get it. 
    ```
    kubectl -n jenkins logs <jenkins-pod-name>
    ```
- Enter the initial password and click next. 
- Install the required plugins and create admin user.
- To create another user go to manage jenkis > create users and enter the required information. Once submitted you can use the new user in jenkins. 
